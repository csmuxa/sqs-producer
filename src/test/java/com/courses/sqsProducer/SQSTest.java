package com.courses.sqsProducer;

import com.courses.sqsProducer.config.SQSTestConfig;
import com.courses.sqsProducer.model.OrderEvent;
import com.courses.sqsProducer.service.EventService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.localstack.LocalStackContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.testcontainers.containers.localstack.LocalStackContainer.Service.SQS;

@SpringBootTest(classes = SQSTestConfig.class)
@Testcontainers
public class SQSTest {

    public static final String QUEUE_NAME = "order-event-test-queue";

    @Autowired
    private EventService service;
    @Autowired
    private QueueMessagingTemplate template;

    @Container
    public static LocalStackContainer localStack =
            new LocalStackContainer(DockerImageName.parse("localstack/localstack:0.10.0"))
                    .withServices(SQS)
                    .withEnv("DEFAULT_REGION", "eu-west-3");

    @BeforeAll
    static void beforeAll() throws IOException, InterruptedException {
        localStack.execInContainer("awslocal", "sqs", "create-queue", "--queue-name", QUEUE_NAME);
    }

    @DynamicPropertySource
    static void dataSourceProperties(DynamicPropertyRegistry registry) {
        registry.add("cloud.aws.end-point.uri", SQSTestConfig::getEndpoint);
    }

    @Test
    @SuppressWarnings("ConstantConditions")
    public void test() {
        service.produce(new OrderEvent()
                .setId(10L)
                .setAction("action")
                .setOrderName("name")
                .setOrderStatus("active"));

        OrderEvent event = template.receiveAndConvert(SQSTestConfig.getEndpoint(), OrderEvent.class);

        assertEquals("action", event.getAction());
        assertEquals(10L, event.getId());
        assertEquals("name", event.getOrderName());
        assertEquals("active", event.getOrderStatus());
    }

}
