package com.courses.sqsProducer.service;

import com.courses.sqsProducer.model.OrderEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class EventService {

    @Value("${cloud.aws.end-point.uri}")
    private String TEST_QUEUE;

    private final QueueMessagingTemplate messagingTemplate;

    public void produce(OrderEvent event) {
            messagingTemplate.convertAndSend(
                    TEST_QUEUE,
                    event,
                    headers(event.getMessageGroupId(), event.getDeduplicationId())
            );
        }

    private Map<String, Object> headers(String messageGroupId, String deduplicationId) {
        Map<String, Object> headers = new HashMap<>();
        headers.put("message-group-id", messageGroupId);
        headers.put("message-deduplication-id", deduplicationId);

        return headers;
    }

}
