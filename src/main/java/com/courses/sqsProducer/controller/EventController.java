package com.courses.sqsProducer.controller;

import com.courses.sqsProducer.model.OrderEvent;
import com.courses.sqsProducer.service.EventService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/event")
@RequiredArgsConstructor
public class EventController {

    private final EventService eventService;

    @PostMapping
    public void postEvent(@RequestBody OrderEvent event) {
        eventService.produce(event);
    }
}
