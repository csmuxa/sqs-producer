package com.courses.sqsProducer.model;

import lombok.Data;

@Data
public class OrderEvent {

    private Long id;
    private String action;
    private String orderName;
    private String orderDescription;
    private String orderStatus;
    private String messageGroupId;
    private String deduplicationId;

}
