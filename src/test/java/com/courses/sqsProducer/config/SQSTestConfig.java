package com.courses.sqsProducer.config;

import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;
import com.courses.sqsProducer.service.EventService;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.courses.sqsProducer.SQSTest.QUEUE_NAME;
import static com.courses.sqsProducer.SQSTest.localStack;
import static org.testcontainers.containers.localstack.LocalStackContainer.Service.SQS;

@Configuration
public class SQSTestConfig {

    @Bean
    public AmazonSQSAsync amazonSQS() {
        return AmazonSQSAsyncClientBuilder.standard()
                .withCredentials(localStack.getDefaultCredentialsProvider())
                .withEndpointConfiguration(localStack.getEndpointConfiguration(SQS))
                .build();
    }

    @Bean
    public QueueMessagingTemplate queueMessagingTemplate() {
        return new QueueMessagingTemplate(amazonSQS());
    }

    @Bean
    public EventService eventService() {
        return new EventService(queueMessagingTemplate());
    }

    public static String getEndpoint() {
        return localStack.getEndpointConfiguration(SQS).getServiceEndpoint() + "/queue/" + QUEUE_NAME;
    }

}